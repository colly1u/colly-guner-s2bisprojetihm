package modele;
import java.awt.*;

/**
 * Cette classe abstraite est la super classe de tous les polygones.
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public abstract class Polygone extends FigureColoree{
	
	/**
	 * Attribut polygon
	 */
	private Polygon poly;
	
	/**
	 * Constructeur vide.
	 */
	public Polygone(){
		super();
	}
	
	/**
	 * M�thode affichant un polygone (fait appel � fillPolygon de la classe Java Polygon).
	 * @param g - graphique
	 */
	public void affiche(Graphics g){
		this.poly=new Polygon();
        for(Point tmp : this.tab_mem){
            this.poly.addPoint(tmp.rendreX(),tmp.rendreY());
        }
        g.setColor(this.couleur);
        g.fillPolygon(this.poly);
        super.affiche(g);	
	}
	
	/**
	 * Cette m�thode retourne vrai si le point dont les coordonn�es sont pass�es en param�tre se trouve � l'int�rieur du polygone.
	 * @param x - abscisse
	 * @param y - ordonn�e
	 */
	public boolean estDedans(int x,int y){
		boolean res;
		res=this.poly.contains(x,y);
		return res;
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le nombre de points dont on a besoin, en g�n�ral, pour la saisie d'un polygone.
	 */
	public int nbClics() {
		return this.nbPoints();
	}
	
	/**
	 * Cette m�thode modifie le polygone conform�ment � un ensemble de points de saisie (ses nouveaux sommets).
	 * @param tab_saisie - tableau de saisie
	 */
	public void modifierPoints(Point[] tab_saisie){
		if( (tab_saisie != null) && (tab_saisie.length == this.nbPoints()) ) {
            this.tab_mem=tab_saisie;
        }
	}

}
