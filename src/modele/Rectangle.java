package modele;

import java.io.Serializable;

/**
 * Classe qui modelise un rectangle
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Rectangle extends Quadrilatere implements Serializable {

    /** Constructeur vide */
    public Rectangle() {
        super();
    }

	/**
	 * M�thode qui retourne le nombre de clics
	 * @return nombre de clics
	 */
    public int nbClics() {
        return 2;
    }

    /**
	 * Cette m�thode modifie le polygone conform�ment � un ensemble de points de saisie (ses nouveaux sommets).
	 * @param tab_saisie - tableau de saisie
	 */
    public void modifierPoints(Point[] tab_saisie) {
        if (tab_saisie != null) {
            if (tab_saisie.length == nbClics()) {
                tab_mem[0] = tab_saisie[0];
            }
            Point p1=new Point(tab_saisie[1].rendreX(), tab_saisie[0].rendreY());
            Point p2=tab_saisie[1];
            Point p3=new Point(tab_saisie[0].rendreX(), tab_saisie[1].rendreY());
            
            
            tab_mem[1] = p1;
            tab_mem[2] = p2;
            tab_mem[3] = p3;
        }
    }


}