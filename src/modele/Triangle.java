package modele;
import java.io.Serializable;

/**
 * M�thode qui mod�lise un triange
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Triangle extends Polygone implements Serializable{

    /**
     * Constructeur par default.
     */
    public Triangle() {
        super();
    }

    /** Nombre de clics a effectuer */
    public int nbPoints() {
        return 3;
    }
}
