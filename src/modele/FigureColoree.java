package modele;
import java.awt.*;

/**
 * Classe qui permet de mod�liser la couleur d'une figure
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public abstract class FigureColoree {
	
	/**
	 * Attribut couleur
	 */
	protected Color couleur;
	
	/**
	 * Attribut constant qui represente une dimension d'un carr�
	 */
	private static final int PERIPHERIE_CARRE_SELECTION=8;
	
	/**
	 * Indique si la figure est selection
	 */
	private boolean selected;
	
	/**
	 * Tableau des points de m�morisation de la figure.
	 */
	protected Point[] tab_mem;
	private static final int TAILLE_CARRE_SELECTION=4;
	
	/**
	 * Constructeur qui initialise les attributs
	 */
	public FigureColoree() {
        this.couleur=Color.BLACK;
        this.tab_mem=new Point[this.nbPoints()];
        this.selected=false;
    }
	
	/**
	 * M�thode d'affichage de la figure Si la figure est s�lectionn�e des petits carr�s sont dessin�s autour des points de m�morisation.
	 * @param g
	 */
	public void affiche(Graphics g) {
        g.setColor(Color.BLACK);
        if(this.selected && this.tab_mem.length != 1) {
        	int dec;
            int x;
            int y;
        	// (for(objet o : object[]) permet a la variable o de parcourir tout le tableau et s'arretera � length du tableau
        	// en prenant l'adresse d'un objet a chaque itineration
            for(Point tmp : this.tab_mem) {
                dec=FigureColoree.TAILLE_CARRE_SELECTION/2;
                x=tmp.rendreX()-dec;
                y=tmp.rendreY()-dec;
                g.fillRect(x,y,FigureColoree.TAILLE_CARRE_SELECTION,FigureColoree.TAILLE_CARRE_SELECTION);
            }
        }
    }
	
	/**
	 * M�thode qui d�tecte un point se trouvant pr�s d'un carr� de s�lection.
	 * @param x - point abscisse
	 * @param y - point ordonee
	 * @return detection du point
	 */
	public int carreDeSelection(int x,int y){
		int indice = -1;
        int i = 0;
        boolean nextX;
        boolean nextY;
        for(Point tmp : this.tab_mem) {
            nextX=Math.abs(tmp.rendreX()-x)<=FigureColoree.PERIPHERIE_CARRE_SELECTION;
            nextY=Math.abs(tmp.rendreY()-y)<=FigureColoree.PERIPHERIE_CARRE_SELECTION;
            if(nextX && nextY) {
                indice=i;
            }
            i++;
        }
        return indice;
    }
	
	/**
	 * M�thode permettant de changer la couleur de la figure.
	 * @param c - couleur a changer
	 */
	public void changeCouleur(Color c){
		this.couleur=c;
	}
	
	/**
	 * Cette m�thode d�s�lectionne la figure.
	 */
	public void deSelectionne(){
		 this.selected=false;
	}
	
	/**
	 * Cette m�thode s�lectionne la figure.
	 */
	public void selectionne() {
        this.selected = true;
    }
	
	/**
	 * Cette m�thode permet d'effectuer une transformation des coordonn�es des points de m�morisation de la figure.
	 * @param dx - point abscisse
	 * @param dy - point ordonnee
	 * @param indice - indice du tableau en m�moire
	 */
	public void transformation(int dx,int dy,int indice){
		this.tab_mem[indice].translation(dx,dy);	
	}
	
	/**
	 * Cette m�thode permet d'effectuer une translation des coordonn�es des points de m�morisation de la figure.
	 * @param dx - point abscisse
	 * @param dy - point ordonnee
	 */
	public void translation(int dx, int dy){
		for(Point p : this.tab_mem) {
            p.incrementerX(dx);
            p.incrementerY(dy);
        }
	}
	
	
	/**
	 * M�thode abstraite qui retourne le nombre de points de m�morisation.
	 * @return nombre de points
	 */
	public abstract int nbPoints();
	
	/**
	 * Cette m�thode retourne vrai si le point dont les coordonn�es sont pass�es en param�tre se trouve � l'int�rieur de la figure.
	 * @param x - point abscisse
	 * @param y - point ordonnee
	 * @return vrai les parametres sont dans la figure
	 */
	public abstract boolean estDedans(int x,int y);
	
	/**
	 * M�thode abstraite qui permet de modifier les points de m�morisation � partir de points de saisie.
	 * @param ps - point de saisie
	 */
	public abstract void  modifierPoints(Point[] ps);
	
	/**
	 * M�thode abstraite qui retourne le nombre de points de saisie (nombre de clics).
	 * @return nombre de clics
	 */
	public abstract int nbClics();
	
	
}
