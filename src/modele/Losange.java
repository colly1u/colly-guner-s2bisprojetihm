package modele;

import java.io.Serializable;

/**
 * Classe qui modelise un losange
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Losange extends Quadrilatere implements Serializable {

   
	
	/**
     * M�thode modifiant les Points afin de cr�er un Losange
     * @param tab_saisie Ensemble de Points definis par l'utilisateur.
     */
    public void modifierPoints(Point[] tab_saisie) {
        if(tab_saisie != null) {
            tab_mem[0] = tab_saisie[0];
            int x=tab_saisie[0].rendreX();
            int y=tab_saisie[0].rendreY();
            int dx=Math.abs(tab_saisie[0].rendreX()-tab_saisie[1].rendreX());
            int dy=Math.abs(tab_saisie[0].rendreY()-tab_saisie[1].rendreY());
           
            Point p1=new Point(x+dx,y+dy);
            Point p2=new Point(x,y+(2*dy));
            Point p3=new Point(x-dx,y+dy);
            
            tab_mem[1]=p1;
            tab_mem[2]=p2;
            tab_mem[3]=p3;
        }

    }

    /**
     * M�thode qui retourne le nombre de clics
     * @return nombre de clics
     */
    public int nbClics() {
        return 3;
    }
    


}
