package modele;

import java.io.Serializable;

/**
 * Classe qui modelise un carre
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Carre extends Rectangle implements Serializable {
	
	/**
	 * M�thode permettant de modifier un ou plusieurs points
	 * @param tab_saisie - tableau de point 
	 */
	public void modifierPoints(Point[] tab_saisie) {
        if (tab_saisie != null) {
            if (tab_saisie.length == nbClics())
                tab_mem[0] = tab_saisie[0];
            int cote = ((Math.abs(tab_saisie[0].rendreX()-tab_saisie[1].rendreX()))+(Math.abs(tab_saisie[0].rendreY()-tab_saisie[1].rendreY())));
            int x = tab_saisie[0].rendreX();
            int y = tab_saisie[0].rendreY();
            Point p1=new Point(x+cote,y);
            Point p2=new Point(x+cote,y+cote);
            Point p3=new Point(x,y+cote);
            
            tab_mem[1]=p1;
            tab_mem[2]=p2;
            tab_mem[3]=p3; 

        }
    }
}