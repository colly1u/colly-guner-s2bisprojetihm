package modele;

/**
 * Classe qui mod�lise un point
 * @author COLLY Florian
 * @author GUNER Yusuf
 */
public class Point {
	
	/**
	 * Abscisse d'un point
	 */
	private int x;
	
	/**
	 * Ordonn�e d'un point
	 */
	private int y;
	
	/**
	 * Constructeur qui initialise les parametres
	 * @param abs - abscisse du point
	 * @param ord - ordonn�e du point
	 */
	public Point(int abs,int ord){
		if(abs<0){
			abs=0;
		}
		if(ord<0){
			ord=0;
		}
		this.x=ord;
		this.y=abs;
	}
	
	/**
	 * M�thode qui calcule la distance
	 * @param p2 
	 * @return distance
	 */
	public double distance(Point p2){
		double distance;
		distance=Math.sqrt((Math.pow((p2.rendreX() - this.x),2)+(Math.pow((p2.rendreY() - this.y),2))));
		return distance;
	}
	
	/**
	 * M�thode qui incr�mente l'abscisse du point
	 * @param incx - valeur a incrementer a x
	 */
	public void incrementerX(int incx){
		this.x=this.x+incx;
	}
	
	/**
	 * M�thode qui incr�mente l'ordonn�e du point
	 * @param incx - valeur a incrementer a y
	 */
	public void incrementerY(int incy){
		this.y=this.y+incy;
	}
	
	/**
	 * M�thode qui modifie l'abscisse
	 * @param x - nouvelle valeur de l'abscisse
	 */
	public void modifierX(int x){
		this.x=x;
	}
	
	/**
	 * M�thode qui modifie l'ordonn�e
	 * @param x - nouvelle valeur de l'ordonn�e
	 */
	public void modifierY(int y){
		this.y=y;
	}
	
	/**
	 * Get qui retourne x
	 * @return x
	 */
	public int rendreX(){
		return (this.x);
	}
	
	/**
	 * Get qui retourne y
	 * @return y
	 */
	public int rendreY(){
		return(this.y);
	}
	
	/**
	 * M�thode qui translate les points
	 * @param dx - abscisse
	 * @param dy - ordonn�e
	 */
	public void translation(int dx,int dy){
		this.x=this.x+dx;
		this.y=this.y+dy;
	}

}
