package modele;

/**
 * Classe qui modelise un quadrilatere
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Quadrilatere extends Polygone {
	
	/**
	 * Constructeur vide.
	 */
	public Quadrilatere(){
		super();
	}
	
	/**
	 * M�thode qui retourne le nombre de clics
	 * @return nombre de clics
	 */
	public int nbPoints() {
		return (4);
	}

}
