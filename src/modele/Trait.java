package modele;
import java.io.Serializable;
import java.awt.*;

/**
 * Classe qui permet de dessiner un trait dans l'interface graphique
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Trait implements Serializable {

	/** X de d�but */
	private int xd;
    /** Y de d�but */
	private int yd;
    /** X de fin */
	private int xf;
    /** Y de fin */
	private int yf;
    /** Couleur du trait */
	private Color couleur;

	/**
	 * Constructeur qui initialise les attributs
	 * @param x0 - abscisse du point 0
	 * @param y0 - ordonn�e du point 0
	 * @param x1 - abscisse du point 1
	 * @param y1 - ordonn�e du point 0
	 * @param c - couleur du trait
	 */
	public Trait(int x0, int y0, int x1, int y1, Color c) {
		this.xd=x0;
		this.yd=y0;
		this.xf=x1;
		this.yf=y1;
		this.couleur=c;
	}

	/** Methode dessinant ce trait */
    public void affiche(Graphics g) {
        g.setColor(this.couleur);
        g.drawLine(this.xd, this.yd, this.xf, this.yf);
    }
 
}
