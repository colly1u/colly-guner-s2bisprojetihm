package controleur;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import modele.FigureColoree;
import vue.DessinFigures;

/**
 * Classe qui permet de manipuler l'interface graphique grace a la souris
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class ManipulateurFormes implements MouseListener, MouseMotionListener, KeyListener {

	/** derni�re  abscisse du coin de la figure */
	private int last_x;
	
	/** derni�re  ordonnee du coin de la figure */
	private int last_y;

	/** Indice de la figure selectionnee */
	private int indSelec;

	/** Indique si la figure est selection */
	private boolean selected ;
	
	/** liste de Figure Color�e */
	private ArrayList<FigureColoree> lfig;


	/**
	 * Constructeur qui initialise la liste et mets la figure n'est pas selectionnee
	 * @param l - liste 
	 */
	public ManipulateurFormes(ArrayList<FigureColoree> l){
		this.lfig =l;
        this.selected=false;
	}

    /** selection d'une Figure */
	public void mouseClicked(MouseEvent e) {
        this.selected=false;
		this.last_x = e.getX();
		this.last_y = e.getY();
		for (int i=0;i< this.lfig.size(); i++){
			if ( this.lfig.get(i).estDedans(e.getX(), e.getY())){
                this.lfig.get(this.indSelec).deSelectionne();
				this.selected=true;
				this.indSelec=i;
                this.lfig.get(i).selectionne();
			}
            ((DessinFigures)e.getSource()).repaint();
		}
	}

	/** d�place une Figure suite a l'evenement de la souris */
	public void mouseDragged(MouseEvent e) {
		if (this.selected ){
			this.lfig.get(this.indSelec).translation(e.getX()-last_x,e.getY()-last_y);
            this.last_x = e.getX();
            this.last_y = e.getY();
		}
        ((DessinFigures)e.getSource()).repaint();
	}

    /**  recupere la figure en manipulation */
    public FigureColoree getFigure() {
        FigureColoree f = null;
        if(selected)
            f = this.lfig.get(indSelec);
        return f;
    }

    /** change la couleur de 
     * la figure selectionnee.
     * @param c Nouvelle Couleur
     */
    public void setColor(Color c) {
        if(selected)
            this.lfig.get(this.indSelec).changeCouleur(c);
    }
    
    /**
     * M�thode de la manipulation du clavier (ici touche suppr)
     * @param e - evenement
     */
    public void keyPressed(KeyEvent e) {
        if((this.selected) && ((e.getKeyCode()==KeyEvent.VK_DELETE))) {
            this.lfig.remove(this.indSelec);
            this.selected=false;
            this.indSelec = 0;
            ((DessinFigures)e.getSource()).repaint();
        }
    }

    /* Non utilis�s */
    public void mouseMoved(MouseEvent arg0) {}
    public void mouseEntered(MouseEvent arg0) {}
    public void mouseExited(MouseEvent arg0) {}
    public void mousePressed(MouseEvent arg0) {}
    public void mouseReleased(MouseEvent arg0) {}
    public void keyReleased(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}
    
    
    
    
}
