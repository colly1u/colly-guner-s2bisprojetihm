package controleur;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import modele.Trait;
import vue.DessinFigures;

/**
 * Classe scribble (manipulation avec la souris)
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Scribble implements MouseListener, MouseMotionListener {

    /** Abscisse d'un clic de souris. */
    private int clic_x;

    /** Ordonn�e d'un clic de souris. */
    private int clic_y;

    /** Couleur du trait � dessiner. */
    private final Color couleur_trait;

	/**
	 * Constructeur de la classe.
	 */
	public Scribble(Color c){
		this.couleur_trait = c;
	}

	/**
	 * M�thode d�butant le trace � main lev�e.
	 * @param e �v�nement clic de souris.
	 */
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			clic_x=e.getX();
			clic_y=e.getY();
		} else if (SwingUtilities.isRightMouseButton(e)) {
			((JComponent)(e.getSource())).removeMouseListener(this);
			((JComponent)(e.getSource())).removeMouseMotionListener(this);
			((JComponent)(e.getSource())).repaint();
		} else if (SwingUtilities.isMiddleMouseButton(e)) {
			((JComponent)(e.getSource())).repaint();
		}
	}

	/**
	 * M�thode effectuant le trace � main lev�e.
	 * @param e �v�nement clic de souris.
	 */
	public void mouseDragged(MouseEvent e) {
        DessinFigures d = ((DessinFigures)(e.getSource()));
		d.ajouteTrace(new Trait(this.clic_x, this.clic_y,
				e.getX(), e.getY(),
				this.couleur_trait));
		clic_x=e.getX();
		clic_y=e.getY();
        d.repaint();
	}

    /** M�thodes des interfaces MouseListener et MouseMotionListener non utilis�es. */
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
    public void mouseMoved(MouseEvent e) {}

}

