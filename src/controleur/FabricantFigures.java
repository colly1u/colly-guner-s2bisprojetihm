package controleur;
import modele.FigureColoree;
import modele.Point;
import vue.DessinFigures;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

/**
 * Classe qui permet la fabrication des figures
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class FabricantFigures implements MouseListener {
	
	//Attributs

	/**
	 * Figure en cours de fabrication.
	 */
	private FigureColoree  figure_en_cours_de_fabrication;
	
	/**
	 * Accumule le nombre de clics de souris.
	 */
	private int	nb_points_cliques;
	
	/**
	 * Tableau contenant des points cr��s � partir de clics de souris.
	 */
	private Point[]	points_cliques;
	
	//Constructeur
	
	/**
	 * Constructeur de la classe.
	 * @param fc - figure vide cr��e dans la classe Dialogue.
	 */
	public FabricantFigures(FigureColoree fc){
		this.figure_en_cours_de_fabrication=fc;
		this.points_cliques=new Point[figure_en_cours_de_fabrication.nbClics()];
		
	}
	
	//M�thodes
	
	
	/**
	 * M�thode impl�mentant la cr�ation d'une figure g�om�trique via des clics de souris.
	 * @param e - MouseEvent
	 */
	public void mousePressed(MouseEvent e){
		this.points_cliques[this.nb_points_cliques] = new Point(e.getX(),e.getY());
        this.nb_points_cliques++;
        if(this.nb_points_cliques == figure_en_cours_de_fabrication.nbClics()) {
            figure_en_cours_de_fabrication.modifierPoints(this.points_cliques);
            ((DessinFigures)e.getSource()).ajoute(figure_en_cours_de_fabrication);
            ((DessinFigures)e.getSource()).repaint();
            figure_en_cours_de_fabrication = null;

            ((DessinFigures)e.getSource()).removeMouseListener(this);
        }
	}
	
	
	/**
	 * M�thode de l'interface MouseListener non utilis�es.
	 * @param e - �v�nement
	 */
	public void mouseEntered(MouseEvent e){
		
	}
	
	/**
	 * M�thode de l'interface MouseExited non utilis�es.
	 * @param e - �v�nement
	 */
	public void mouseExited(MouseEvent e){
		
	}
	
	/**
	 * M�thode de l'interface MouseReleased non utilis�es.
	 * @param e - �v�nement
	 */
	public void mouseReleased(MouseEvent e){
		
	}
	
	/**
	 * M�thode de l'interface MouseClicked non utilis�es
	 * @param e - �v�nement
	 */
	public void mouseClicked(MouseEvent e){
		
	}
	
}
