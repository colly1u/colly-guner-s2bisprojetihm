package main;
import vue.*;
import java.awt.*;
import java.io.Serializable;
import javax.swing.*;

/**
 * Classe s'occupant de construire  la fenetre principale
 * du projet et de l'�x�cuter
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class Fenetre extends JFrame implements Serializable {
	
	/**
	 * Attribut qui permet de dessiner dans la fenetre.
	 */
	private DessinFigures dessin;
	
	/**
	 * Constructeur qui initialise les parametres
	 * @param nom - nom de la fenetre
	 * @param hauteur - hauteur de la fenetre
	 * @param largeur - largeur de la fenetre
	 */
	public Fenetre(String nom,int hauteur,int largeur){
		super(nom);
		JFrame fenetre = new JFrame(nom);
		JPanel panel = new JPanel();
		fenetre.setPreferredSize(new Dimension(hauteur,largeur));
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setLocationRelativeTo(null);
        
        this.dessin = new DessinFigures();
        
        panel.setLayout(new BorderLayout());
		dessin = new DessinFigures();
        PanneauChoix panneau = new PanneauChoix(dessin);
        fenetre.setJMenuBar(new BarreMenu(dessin));
		panel.add(dessin,BorderLayout.CENTER);
		panel.add(panneau, BorderLayout.NORTH);
		fenetre.setContentPane(panel);
		dessin.requestFocus();
		fenetre.pack();
        fenetre.setVisible(true);
	}
	
	
	
	
	
	
	
	
	/**
     * M�thode principale � lancer.
     * @param args
     */
	public static void main ( String args [] ){
		Fenetre f = new Fenetre("Figures Geometriques",600,700);
		
	}

}
