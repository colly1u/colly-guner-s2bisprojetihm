package vue;
import modele.*;

//Plusieur Rectangle possible ==> exportation explicite
import modele.Rectangle;

import java.awt.*;

import javax.swing.*;

import controleur.Scribble;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe pour la construction de l'interface utilisateur.
 * @author COLLY Florian(S2bis)
 * @author GUNER Yusuf(S2bis)
 */
public class PanneauChoix extends JPanel  {
	
	
	/**
	 * Il s'agit du dessin
	 */
	private DessinFigures dessin;
	
	/**
	 * Figure � construire
	 */
	private FigureColoree fc;
	
	private JComboBox menuFig,menuCoul;
	private JButton effacer = new JButton("Effacer");
	private final String[] FIGURES = {"Quadrilatere","Carre","Losange","Triangle","Rectangle"};
	private final String[] COULEURS = {"Rouge","Vert","Bleu","Noir","Rose","Jaune","Orange"};
	
	 
	
	
	
	
	
	//Constructeur
	
	/**
	 * Constructeur de la classe
	 */
	public PanneauChoix(DessinFigures dessins){
		super();
		this.dessin=dessins;
		//Creation des radiosButon
				this.setLayout(new BorderLayout());
				ButtonGroup rg = new ButtonGroup();
				final JRadioButton butonFigure = new JRadioButton("Nouvelle figure");
		        butonFigure.setToolTipText("Dessine une Figure predefinis");
				final JRadioButton butonTrace = new JRadioButton("Trace a main levee");
		        butonTrace.setToolTipText("Dessine a la souris");
				final JRadioButton butonManip = new JRadioButton("Manipulation");
		        butonManip.setToolTipText("Edite les figures dessinees");
		        
		        
		        //Ajout des bouton au panneau
		        rg.add(butonFigure);
				rg.add(butonTrace);
				rg.add(butonManip);
				
				
				//Creation menu couleur avec listeners
				this.menuCoul = new JComboBox(COULEURS);
		        menuCoul.setToolTipText("Liste des couleurs disponibles");

				this.menuCoul.addActionListener(new ActionListener(){
					public void actionPerformed ( ActionEvent e){
						if(butonFigure.isSelected())
						{
							dessineFigure();
						}
						if(butonTrace.isSelected()) {
							Scribble s = new Scribble(determineCouleur(menuCoul.getSelectedIndex()));
							dessin.supprimeListerner();
							dessin.activeScribble(s);
						}
		                if(butonManip.isSelected()) {
		                    dessin.getManipulateur().setColor(determineCouleur(menuCoul.getSelectedIndex()));
		                    dessin.repaint();
		                }
					}

				});
				this.menuCoul.setEnabled(false);
				
				
				
				//Creation du menu Figures avec son listener et celui de newfigure
				menuFig= new JComboBox(FIGURES);
				menuFig.setToolTipText("Liste des figures disponibles");
				menuFig.setEnabled(false);
				
				butonFigure.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
		                if(butonFigure.isSelected()) {
		                    menuFig.setEnabled(true);
		                    menuCoul.setEnabled(true);
		                    dessineFigure();
		                }
		            }
		        });
				
				menuFig.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dessineFigure();
					}
				});
				
				
				//Listener manip
				
				butonManip.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						fc = null;
						dessin.supprimeListerner();
						dessin.activeManipulationsSouris();
						menuFig.setEnabled(false);
					}
				});
				
				
				//Listener trace
				butonTrace.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if ( butonTrace.isSelected()) {
							menuCoul.setEnabled(true);
							dessin.supprimeListerner();
							menuFig.setEnabled(false);
							Scribble s = new Scribble(determineCouleur(menuCoul.getSelectedIndex()));
							dessin.activeScribble(s);
						}
					}

				});
				
				//listener effacer
		        this.effacer.setBackground(Color.WHITE);
		        effacer.setToolTipText("Efface la zone de dessin");
		        this.effacer.setFocusPainted(false);
				effacer.addActionListener( new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						dessin.effacer();				
					}        	
				});	
				
				//Ajout de tout les composants
				JPanel ligne1 = new JPanel();
				ligne1.add(butonFigure);
				ligne1.add(butonTrace);
				ligne1.add(butonManip);
				this.add(ligne1,BorderLayout.NORTH);
				
				JPanel ligne2 = new JPanel();
				ligne2.add(menuFig);
				ligne2.add(menuCoul);
				ligne2.add(effacer);
				this.add(ligne2,BorderLayout.SOUTH);
		
	}
	
	//M�thodes
	
	/**
	 * M�thode impl�mentant la cr�ation d'une forme g�om�trique.
	 * @param index - indice repr�sentant la figure � construire.
	 * @return une figure color�e de type choisi par l'utilisateur.
	 */
	private FigureColoree creerFigure(int index){
		FigureColoree f;
		switch (index) {
		case 0:
			f = new Quadrilatere();
			break;
		case 1:
			f = new Carre();
			break;
		case 2:
			f = new Losange();
			break;
		case 3:
			f = new Triangle();
			break;
		case 4:
			f = new Rectangle();
			break;
		default:
			f = new Quadrilatere();
		}
		return f;
	}
	
	
	/**
	 * M�thode d�terminant la couleur � utiliser.
	 * @param index - indice repr�sentant la couleur � utiliser.
	 * @return la couleur choisie par l'utilisateur
	 */
	private Color determineCouleur(int index){
		Color c;
		switch(index){
		case 0 :
			c = Color.RED;
			break;
		case 1 :
			c = Color.GREEN;
			break;
		case 2 :
			c = Color.BLUE;
			break;
		case 3:
			c = Color.BLACK;
			break;
		case 4:
			c=Color.PINK;
			break;
		case 5:
			c=Color.YELLOW;
			break;
		case 6:
			c=Color.ORANGE;
			break;
		default:
			c = Color.BLACK;

		}
		return c ;
		
	}
	
	/**
	 * Dessine une figure
	 */
	private void dessineFigure() {
		this.fc = null;
		this.dessin.supprimeListerner();
		this.fc = creerFigure(this.menuFig.getSelectedIndex());
		this.fc.changeCouleur(determineCouleur(this.menuCoul.getSelectedIndex()));
		this.dessin.construit(this.fc);
	}
	
	
	
	
}
