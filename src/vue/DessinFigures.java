package vue;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;

import controleur.FabricantFigures;
import controleur.ManipulateurFormes;
import modele.FigureColoree;
import controleur.Scribble;
import modele.Trait;

/**
 * M�thode qui permet de dessiner une figure quelconque
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class DessinFigures extends JPanel implements Serializable {

	/** attribut correspondant � la liste de figures color�es */
	private ArrayList<FigureColoree> lfig;

	/** Liste des trac�es a main lev�s */
	private ArrayList<Trait> traces;

	/**
	 * Attribut de la fabrication des figuresz
	 */
	private FabricantFigures fFig;
	
	/**
	 * Scribble
	 */
	private Scribble s;
	
	/**
	 * Attribut qui permet de manipuler les formes d'un figure
	 */
	private ManipulateurFormes mf;


	/**
	 * Constructeur vide : dessin ne contenant aucune figure.
	 */
	public DessinFigures() {
		this.lfig = new ArrayList<FigureColoree>();
		this.traces = new ArrayList<Trait>();
		
		this.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				for (FigureColoree f : lfig) {
					if (f.estDedans(e.getX(), e.getY()))
						f.selectionne();
					else
						f.deSelectionne();
				}
				repaint();
			}

			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}

		});
	}

	/**
	 * M�thode graphique
	 * @param g - graphique
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		this.setBackground(Color.white);

		for ( FigureColoree f : this.lfig){
			f.affiche(g);
		}
		for ( Trait t : this.traces){
			t.affiche(g);
		}
	}

	
	/**
	 * M�thode qui permet l'ajout d'une figure
	 * @param fc - figure a ajouter
	 */
	public void ajoute ( FigureColoree fc){
		this.lfig.add(fc);
	}

	/**
	 * Permet l'ajout d'un trait
	 * @param t - trait
	 */
	public void ajouteTrace(Trait t) {
		this.traces.add(t);
	}

	
	/**
	 * Construit une figure
	 * @param fc - figure a construire
	 */
	public void construit( FigureColoree fc){
		this.fFig = new FabricantFigures(fc);
		this.addMouseListener(this.fFig);
	}

    /**
     * M�thode supprimant les Listeners des figures.
     */
    public void supprimeListerner() {
        for(FigureColoree f : this.lfig) {
            f.deSelectionne();
        }
        
        this.removeMouseListener(this.fFig);
        this.removeMouseListener(this.mf);
        this.removeMouseMotionListener(this.mf);
        this.removeMouseListener(this.s);
        this.removeMouseMotionListener(this.s);
        this.removeKeyListener(this.mf);
        this.repaint();
    }

   /**
    * Active la manipulation
    */
    public void activeManipulationsSouris() {
        this.supprimeListerner();
        this.mf = new ManipulateurFormes(this.lfig);
        this.addMouseListener(this.mf);
        this.addMouseMotionListener(this.mf);
        this.addKeyListener(this.mf);
        
        //key event
        this.requestFocus();
    }

	/**
	 * Methode activant le mode "Trac�e a main lev�e
	 * @param s Nouveau Scribble
	 */
	public void activeScribble(Scribble scri) {
		this.supprimeListerner();
		this.s = scri;
		this.addMouseListener(this.s);
		this.addMouseMotionListener(this.s);
	}

	/**
	 * M�thode recup�rant le Controleur Manipulateur
	 * @return Manipulateur en cours
	 */
	public ManipulateurFormes getManipulateur() {
		return mf;
	}

	/**
	 * M�thode qui permet d'effacer le contenu d'un dessin
	 */
	public void effacer(){
		this.traces = new ArrayList<Trait>();
		this.lfig = new ArrayList<FigureColoree>();
		this.repaint();
	}


	/**
	 * M�thode de sauvegarde
	 * @param f - fichier � sauvegarder
	 */
	public void sauvegarder(File f){
		try{
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
            os.writeObject(this.traces);
            os.writeObject(this.lfig);
			os.writeObject(this.getBackground());
			os.close();
		}catch(IOException e){
			System.out.println("Erreux dans la sauvegarde du fichier");
		}catch(Exception e){
			System.out.println("Erreur inconnue");
			e.printStackTrace();
		}
	}

	/**
	 * M�thode de chargement d'un fichier
	 * @param f - fichier � charger
	 */
	public void charger(File f){
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
			this.traces = (ArrayList<Trait>)ois.readObject();
			this.lfig = (ArrayList<FigureColoree>)ois.readObject();
			this.setBackground((Color)ois.readObject());
			ois.close();
			this.repaint();
		}catch(IOException e){
			System.out.println("Erreux dans le chargement du fichier");
		}catch(Exception e){
			System.out.println("ErreurInconnue");
			e.printStackTrace();
		}
	}
}