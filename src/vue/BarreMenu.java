package vue;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

/**
 * Classe qui represente la barre de menu
 * @author COLLY Florian(s2bis)
 * @author GUNER Yusuf(s2bis)
 */
public class BarreMenu extends JMenuBar implements ActionListener  {
	
	/**
	 * Menu charger
	 */
    private JMenuItem charger;
    
	/**
	 * Menu charger
	 */
    private JMenuItem sauvegarder;
    
    /**
     * Attribut qui permet le dessin
     */
    private DessinFigures dessin ;
    
    /**
     * Constructeur de la classe
     * @param d - dessin
     */
    public BarreMenu(DessinFigures d){
		this.dessin=d;
		JMenu menu=new JMenu("fichier");
		this.add(menu);
		this.charger=new JMenuItem("charger");
		menu.add(this.charger);
		charger.addActionListener(this);
		this.sauvegarder=new JMenuItem("Sauvegarder");
		menu.add(this.sauvegarder);
		sauvegarder.addActionListener(this);
	}

    /**
     * M�thode qui permet l'interception des actions de l'utilisateur
     * @param e - evenement
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			if ( e.getSource() == this.charger){
				JFileChooser choix = new JFileChooser(new File(""));
				if ( choix.showOpenDialog(this.dessin) == JFileChooser.APPROVE_OPTION){
					File fichier = choix.getSelectedFile();
					this.dessin.charger(fichier);
				}

			}
            else {

                if(e.getSource()  == this.sauvegarder) {
                    JFileChooser choix = new JFileChooser(new File(""));
                    if ( choix.showSaveDialog(this.dessin) == JFileChooser.APPROVE_OPTION){
                        File fichier = choix.getSelectedFile();	
                        this.dessin.sauvegarder(fichier);
                    }
                }
               
            }

		}catch(Exception exc){
			System.out.println("Probl�me avec la sauvegarde ou le chargement du fichier");
			exc.printStackTrace();
		}
	}

}
